
public class Jakobian {

    double ElementId;

    double xE,xN,yE,yN;
    double x1,x2,x3,x4;
    double y1,y2,y3,y4;

    double [] xArray = new double[4];
    double [] yArray = new double[4];

    public Jakobian(double xE, double xN, double yE, double yN) {
        this.xE = xE;
        this.xN = xN;
        this.yE = yE;
        this.yN = yN;
    }

    public Jakobian countJ(UniversalElement uEL, Element EL, int PunktCalkowania) {
        xE = 0;
        xN = 0;
        yE = 0;
        yN = 0;

        xArray[0] = EL.getNode(1).getX();
        xArray[1] = EL.getNode(2).getX();
        xArray[2] = EL.getNode(3).getX();
        xArray[3] = EL.getNode(4).getX();

        yArray[0] = EL.getNode(1).getY();
        yArray[1] = EL.getNode(2).getY();
        yArray[2] = EL.getNode(3).getY();
        yArray[3] = EL.getNode(4).getY();

        ElementId = EL.getId();

        if(PunktCalkowania == 1 ) {
            for (int i = 0; i < 4; i++) {
                xE += uEL.MatrixE[0][i] * xArray[i];
                yE += uEL.MatrixE[0][i] * yArray[i];
                xN += uEL.MatrixN[0][i] * xArray[i];
                yN += uEL.MatrixN[0][i] * yArray[i];
            }

            return this;
        }

       else if(PunktCalkowania == 2)
        {
            for (int i = 0; i < 4; i++) {
                xE += uEL.MatrixE[1][i] * xArray[i];
                yE += uEL.MatrixE[1][i] * yArray[i];
                xN += uEL.MatrixN[1][i] * xArray[i];
                yN += uEL.MatrixN[1][i] * yArray[i];
            }

            return this;
        }

       else if(PunktCalkowania == 3)
        {
            for (int i = 0; i < 4; i++) {
                xE += uEL.MatrixE[2][i] * xArray[i];
                yE += uEL.MatrixE[2][i] * yArray[i];
                xN += uEL.MatrixN[2][i] * xArray[i];
                yN += uEL.MatrixN[2][i] * yArray[i];
            }

            return this;
        }

       else if(PunktCalkowania == 4)
        {
            for (int i = 0; i < 4; i++) {
                xE += uEL.MatrixE[3][i] * xArray[i];
                yE += uEL.MatrixE[3][i] * yArray[i];
                xN += uEL.MatrixN[3][i] * xArray[i];
                yN += uEL.MatrixN[3][i] * yArray[i];
            }
            
            return this;
        }

        return this;
    }

    public static void printJakobian(Jakobian J)
    {
        System.out.println(" ElementID = " +J.ElementId+ "\n" + J.xE +"\t"+ J.yE +"\n" + J.xN +"\t" +J.yN+"\n");
    }
}
