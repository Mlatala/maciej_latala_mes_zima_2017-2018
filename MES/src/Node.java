
public class Node {

	public double x;
	public double y;
	public int id;
	double temperature;		
	boolean status;
	
	public Node()
	{
		x=-1;
		y=-1;
		id=-1;
		temperature=0;
		status=false;
	}
	
	public Node(double x,double y, int id,int temperature,boolean status)
	{
		this.x=x;
		this.y=-y;
		this.id=id;
		this.temperature=temperature;
		this.status=status;
	}
	
	public double getX(){		
		return x;
	}
	public double getY(){		
		return y;
	}
	public int getId(){		
		return id;
	}
	public double getTemperature(){		
		return temperature;
	}
	public boolean getStatus(){		
		return status;
	}
	
	public void setX(double x){		
		this.x=x;
	}
	public void setY(double y){		
		this.y=y;
	}
	public void setId(int id){		
		this.id=id;
	}
	public void setTemperature(double temperature){		
		this.temperature=temperature;
	}
	public void setStatus(boolean status){		
		this.status=status;
	}
}
